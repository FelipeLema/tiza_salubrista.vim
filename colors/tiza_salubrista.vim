" Vim color scheme
" Name:         tiza_salubrista
" Author:       Konnor Rogers <konnor7414@gmail.com>
" Version:      2.0

" Based on the VividChalk colorscheme made by Tpope which is based on Vibrant Ink
" Distributable under the same terms as Vim itself (see :help license)

if has("gui_running") && &background !=# 'dark'
    set background=dark
endif
hi clear
if exists("syntax_on")
   syntax reset
endif

let colors_name = "tiza_salubrista"



hi link rubyDefine          Keyword
hi link rubySymbol          Constant
hi link rubyEval            rubyMethod
hi link rubyException       rubyMethod
hi link rubyInclude         rubyMethod
hi link rubyMacro           rubyMethod
hi link rubyStringDelimiter rubyString
hi link rubyRegexp          Regexp
hi link rubyRegexpDelimiter rubyRegexp

hi link javascriptRegexpString  Regexp

hi link diffAdded               String
hi link diffRemoved             Statement
hi link diffLine                PreProc
hi link diffSubname             Comment

hi Normal guifg=#EEEEEE  ctermfg=White
if &background == "light" || has("gui_running")
    hi Normal guibg=Black ctermbg=Black
else
    hi Normal guibg=Black ctermbg=NONE
endif

highlight Ignore        ctermfg=Black
highlight WildMenu      guifg=Black   guibg=#ffff00 gui=bold ctermfg=Black ctermbg=Yellow cterm=bold
highlight Cursor        guifg=Black guibg=White ctermfg=Black ctermbg=White
highlight NonText       guifg=#404040 ctermfg=8
highlight SpecialKey    guifg=#404040 ctermfg=8
highlight Directory     none
high link Directory     Identifier
highlight ErrorMsg      guibg=Red ctermbg=DarkRed guifg=NONE ctermfg=NONE
highlight Search        guifg=NONE    ctermfg=NONE gui=none cterm=none
highlight Search        guibg=#353535 ctermbg=DarkBlue
highlight IncSearch     guifg=White guibg=Black ctermfg=White ctermbg=Black
highlight MoreMsg       guifg=#00AA00 ctermfg=Green
highlight LineNr        guifg=#DDEEFF ctermfg=White
highlight LineNr        guifg=#222222 ctermfg=DarkBlue
highlight Question      none
high link Question      MoreMsg
highlight Title         guifg=Magenta ctermfg=Magenta
highlight VisualNOS     gui=none cterm=none
highlight Visual        guibg=#20202d ctermbg=LightBlue
highlight VisualNOS     guibg=#363636 ctermbg=DarkBlue
highlight WarningMsg    guifg=Red ctermfg=Red
highlight Error         ctermbg=DarkRed
highlight SpellBad      ctermbg=DarkRed
" FIXME: Comments
highlight SpellRare     ctermbg=DarkMagenta
highlight SpellCap      ctermbg=DarkBlue
highlight SpellLocal    ctermbg=DarkCyan

highlight Folded        guibg=#110077 ctermbg=DarkBlue
highlight Folded        guifg=#aaddee ctermfg=LightCyan
highlight FoldColumn    none
high link FoldColumn    Folded
highlight DiffAdd       ctermbg=4 guibg=DarkBlue
highlight DiffChange    ctermbg=5 guibg=DarkMagenta
highlight DiffDelete    ctermfg=12 ctermbg=6 gui=bold guifg=Blue guibg=DarkCyan
highlight DiffText      ctermbg=DarkRed
highlight DiffText      cterm=bold ctermbg=9 gui=bold guibg=Red

highlight TabLine       gui=underline cterm=underline
highlight TabLine       guifg=#bbbbbb ctermfg=LightGrey
highlight TabLine       guibg=#333333 ctermbg=DarkGrey
highlight TabLineSel    guifg=White guibg=Black ctermfg=White ctermbg=Black
highlight TabLineFill   gui=underline cterm=underline
highlight TabLineFill   guifg=#bbbbbb ctermfg=LightGrey
highlight TabLineFill   guibg=#808080 ctermbg=Grey

hi Type gui=none
hi Statement gui=none
if !has("gui_mac")
    " Mac GUI degrades italics to ugly underlining.
    hi Comment gui=italic
endif
hi Identifier cterm=none
" Commented numbers at the end are *old* 256 color values
highlight Comment    guifg=#9933CC ctermfg=DarkMagenta
" 26 instead?
highlight Constant   guifg=#339999 ctermfg=DarkCyan
highlight String     guifg=#66FF00 ctermfg=LightGreen
highlight Identifier guifg=#FFCC00 ctermfg=Yellow
highlight Statement  guifg=#FF6600 ctermfg=Brown
highlight PreProc    guifg=#AAFFFF ctermfg=LightCyan
highlight Type       guifg=#AAAA77 ctermfg=Grey
highlight Special    guifg=#33AA00 ctermfg=DarkGreen
highlight Regexp     guifg=#44B4CC ctermfg=DarkCyan
highlight rubyMethod guifg=#DDE93D ctermfg=Yellow

" My modifications

highlight StatusLine   guibg=#aabbee guifg=#000000 ctermbg=14 ctermfg=0 cterm=NONE gui=NONE
highlight StatusLineNC guibg=#aabbee guifg=#000010 ctermbg=14 ctermfg=0 cterm=inverse gui=inverse

highlight MatchParen ctermbg=8 ctermfg=2 cterm=NONE guibg=#111111 guifg=#66FF00 gui=NONE

" Various columns
highlight VertSplit ctermbg=8 ctermfg=8 guibg=#111111 guifg=#111111
highlight ColorColumn ctermbg=8 guibg=#111111
highlight CursorColumn ctermbg=8 guibg=#111111
highlight SignColumn ctermbg=NONE guibg=NONE
highlight CursorLine ctermbg=8 guibg=#111111 cterm=NONE gui=NONE

" Autocompletion
highlight Pmenu      ctermbg=00 ctermfg=15 guibg=#000010  guifg=#ffffff gui=NONE cterm=NONE
highlight PmenuSel   ctermbg=05 ctermfg=07 guibg=#AA1BF2  guifg=#ffffff gui=NONE cterm=NONE
highlight PmenuSbar  ctermbg=07 guibg=#FFFFFF
highlight PmenuThumb ctermbg=15 guibg=#808080

highlight WhiteSpace ctermfg=15 guifg=#808080

highlight link NERDTreeDir Constant
